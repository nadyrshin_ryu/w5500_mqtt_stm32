#include "misc.h"
#include "W5500_driver.h"
#include "wizchip_conf.h"
#include <stdio.h>
#include <delay.h>
#include <spim.h>
#include <gpio.h>

extern wiz_NetInfo gWIZNETINFO;
static SPI_TypeDef* W5500_SPIx;


void W5500_Reset(void)
{
  GPIO_ResetBits(W5500_RESET_PORT, W5500_RESET_PIN);
  delay_us(500);
  GPIO_SetBits(W5500_RESET_PORT, W5500_RESET_PIN);
  delay_ms(1);
}

void W5500_Init(SPI_TypeDef* SPIx)
{
  W5500_SPIx = SPIx;
  
  // ������������� SPI
  spim_init(SPIx, 8);

  // CS Pin
  gpio_PortClockStart(W5500_CS_PORT);
  gpio_SetGPIOmode_Out(W5500_CS_PORT, W5500_CS_PIN);
  // Reset Pin
  gpio_PortClockStart(W5500_RESET_PORT);
  gpio_SetGPIOmode_Out(W5500_RESET_PORT, W5500_RESET_PIN);
  // INT Pin
  //gpio_PortClockStart(W5500_INT_PORT);
  //gpio_SetGPIOmode_In(W5500_INT_PORT, W5500_INT_PIN, gpio_PullUp);

  W5500_Reset();
}

void W5500_chipInit(void)
{
  uint8_t temp;
  uint8_t W5500FifoSize[2][8] = {{2, 2, 2, 2, 2, 2, 2, 2, }, {2, 2, 2, 2, 2, 2, 2, 2}};

  W5500DeSelect();

  /* spi function register */
  reg_wizchip_spi_cbfunc(W5500ReadByte, W5500WriteByte);

  /* CS function register */
  reg_wizchip_cs_cbfunc(W5500Select, W5500DeSelect);

  if (ctlwizchip(CW_INIT_WIZCHIP, (void*)W5500FifoSize) == -1)
    printf("W5500 initialized fail.\r\n");
  
  //check phy status
  do
  {
    if (ctlwizchip(CW_GET_PHYLINK, (void*)&temp) == -1)
    {
      printf("Unknown PHY link status.\r\n");
    }
  } while (temp == PHY_LINK_OFF);
}

void W5500WriteByte(uint8_t byte)
{
  SPI_SendRecvByte(W5500_SPIx, byte);
}

uint8_t W5500ReadByte(void)
{
  return SPI_SendRecvByte(W5500_SPIx, 0xFF);
}

void W5500Select(void)
{
  GPIO_ResetBits(W5500_CS_PORT, W5500_CS_PIN);
}

void W5500DeSelect(void)
{
  GPIO_SetBits(W5500_CS_PORT, W5500_CS_PIN);
}


